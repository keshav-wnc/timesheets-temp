"use strict";
/******************************************************************************
 *
 * Copyright (c) 2019-2021 ServiceActivator Inc. All Rights Reserved.
 *
 * Unauthorized copying of the code in this file, completely or partially,
 * via any medium is strictly prohibited.
 *
 * Proprietary and Confidential.
 *
 * Written by 'Keshav Gupta' <keshav.gupta@wakencode.com>, February 2021
 *
 *****************************************************************************/
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const utilities_1 = require("./utils/utilities");
const constants_1 = require("./utils/constants");
const handleException_1 = require("./utils/handleException");
const db_1 = require("./db/db");
const createExcel_1 = require("./utils/createExcel");
const moment_1 = __importDefault(require("moment"));
const lodash_1 = __importDefault(require("lodash"));
const importLazy = require('import-lazy')(require); //import-lazy
/**************************************************************/
//Environment variable
const DATABASE = process.env.DATABASE || "";
const DATABASE_STAGE = process.env.DATABASE_STAGE || "";
const DATABASE_ROLE = process.env.DATABASE_ROLE || "";
const FILE_NAME = process.env.FILE_NAME || "";
/**************************************************************/
//Require postgres layer 
const databaseLayer = importLazy("/opt/nodejs/postgres/index");
//Require aws layer utilities
const awsLayer = importLazy("/opt/nodejs/utilities/aws/index");
/**************************************************************/
let loggingEnabled = (0, utilities_1.isLoggingEnabled)();
/**************************************************************/
const handler = (event) => __awaiter(void 0, void 0, void 0, function* () {
    //API response format
    let apiResponse = {
        statusCode: 400,
        isBase64Encoded: false,
        body: "{}"
    };
    //Error response format
    let errorResponseBody = {
        "errorCode": 901,
        "errorDescription": ""
    };
    /**************************************************************/
    // let userAttribute = null
    let userAttribute;
    if (event.requestContext.authorizer) {
        userAttribute = event.requestContext.authorizer.claims;
    }
    /**************************************************************/
    //Validate user attribute
    try {
        const AWS = awsLayer.AWS;
        //Validate  user detail
        if (!AWS.isUserAttributesValid(userAttribute))
            throw new Error(constants_1.ERROR_CODES.UNAUTHORIZED);
    }
    catch (exception) {
        console.log(`Exception during userAttribute validation: ${exception}`);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    if (loggingEnabled)
        console.log(`User attributes validated successfully`);
    /**************************************************************/
    let validQueryParams;
    try {
        //Validate query params
        validQueryParams = (0, utilities_1.getValidQueryParams)(event.queryStringParameters);
    }
    catch (exception) {
        console.log(`Exception while validating query params: ${exception}`);
        console.log(`Query Params: ${JSON.stringify(event.queryStringParameters)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    if (loggingEnabled)
        console.log(`Query params validated successfully`);
    /**************************************************************/
    //Initialize connection object
    let postgres;
    try {
        const Database = databaseLayer.Database;
        //Initialize postgres
        postgres = yield Database.initialize({
            database: DATABASE,
            stage: DATABASE_STAGE,
            role: DATABASE_ROLE
        });
    }
    catch (exception) {
        //We log the error returned, but we do not send it in response
        console.log(`Exception during initialize connection object: ${exception}`);
        console.log(`Database: ${DATABASE}`);
        console.log(`Database stage: ${DATABASE_STAGE}`);
        console.log(`Database role: ${DATABASE_ROLE}`);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    if (loggingEnabled)
        console.log(`Connection object initialization successfully`);
    /**************************************************************/
    let accountId = userAttribute["custom:accountId"];
    let userData;
    let userIdStr = '';
    try {
        //Fetch subscriber data
        let queryString = (0, db_1.getQueryToFetchUserData)(accountId);
        userData = yield postgres.query(queryString);
        if (userData.length == 0)
            throw new Error("No user record found");
        userData.map((eachUser) => {
            userIdStr = `${userIdStr}'${eachUser.id}',`;
        });
        userIdStr = userIdStr.slice(0, -1);
    }
    catch (exception) {
        console.log(`Exception while fetching subscribers data: ${exception}`);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    if (loggingEnabled)
        console.log(`User data fetched successfully`);
    /**************************************************************/
    let defaultShiftStartTime = '';
    let subscriberName = '';
    try {
        //Fetch subscriber data
        let queryString = (0, db_1.getQueryToFetchSubscriberData)(accountId);
        let subscriberData = yield postgres.query(queryString);
        //if timesheet is not enabled then throw error
        if (subscriberData[0].is_timesheet_enabled == false)
            throw new Error(constants_1.ERROR_CODES.TIMESHEET_NOT_ENABLED);
        subscriberName = subscriberData[0].name;
        defaultShiftStartTime = subscriberData[0].default_shift_start_time;
    }
    catch (exception) {
        console.log(`Exception while fetching subscribers data: ${exception}`);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    if (loggingEnabled)
        console.log(`Subscriber data fetched successfully`);
    /**************************************************************/
    let isOverTimeEnabled = false;
    try {
        //Fetch timesheet configs of start date to get payroll cycle
        let queryString = (0, db_1.getQueryToFetchTimesheetConfigs)(accountId);
        let timesheetConfigs = yield postgres.query(queryString);
        if (timesheetConfigs.length == 0)
            throw new Error(constants_1.ERROR_CODES.TIMESHEET_CONFIGS_NOT_FOUND);
        isOverTimeEnabled = timesheetConfigs[0].is_over_time_enabled;
    }
    catch (exception) {
        console.log(`Exception while fetching timesheet configs: ${exception}`);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    /**************************************************************/
    let availableUserIdString = '';
    let availableUsersIdSet = new Set();
    try {
        //Fetch timesheet records for given start date and end date
        let queryString = (0, db_1.getQueryToFetchTimesheetRecords)(userIdStr, validQueryParams);
        let timesheetRecords = yield postgres.query(queryString);
        //If no record found then throw error
        if (timesheetRecords.length == 0)
            throw new Error(constants_1.ERROR_CODES.TIMESHEET_RECORDS_NOT_FOUND);
        timesheetRecords.map((eachUser) => {
            availableUserIdString = `${availableUserIdString}'${eachUser.user_id}',`;
            availableUsersIdSet.add(eachUser.user_id);
        });
        availableUserIdString = availableUserIdString.slice(0, -1);
    }
    catch (exception) {
        console.log(`Exception while fetching timesheet records: ${exception}`);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    if (loggingEnabled)
        console.log(`Timesheet records fetched successfully`);
    /**************************************************************/
    let workDayBreaks;
    try {
        //Fetch workday breaks records for given start date and end date
        let queryString = (0, db_1.getQueryToFetchWorkDayBreaksRecords)(availableUserIdString, validQueryParams, accountId);
        workDayBreaks = yield postgres.query(queryString);
    }
    catch (exception) {
        console.log(`Exception while fetching workDaybreaks records: ${exception}`);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    if (loggingEnabled)
        console.log(`Work day breaks records fetched successfully`);
    /**************************************************************/
    let workDayRecords;
    try {
        //Fetch workday records for given start date and end date
        let queryString = (0, db_1.getQueryToFetchWorkDayRecords)(availableUserIdString, validQueryParams, accountId);
        workDayRecords = yield postgres.query(queryString);
    }
    catch (exception) {
        console.log(`Exception while fetching workDaybreaks records: ${exception}`);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    if (loggingEnabled)
        console.log(`Work day records fetched successfully`);
    /**************************************************************/
    //Get users which has timesheets records
    let userDataForRecordsHavingTimesheetsRecords = lodash_1.default.filter(userData, eachUser => {
        return availableUsersIdSet.has(eachUser.id);
    });
    let driveEvents;
    try {
        //Find work order according to input endDate which has non nullable day_end_date_time
        let endDateNonNullableWorkday = lodash_1.default.findLast(workDayRecords, (record) => {
            return record.day_end_date_time != null;
        });
        //If no work orders found or the given work orders have nullable day_end_date_time then throw error
        if (!endDateNonNullableWorkday)
            throw new Error(constants_1.ERROR_CODES.INVALID_WORKDAYS_FOR_GIVEN_RANGE);
        let range_day_end_date_time = (0, moment_1.default)(endDateNonNullableWorkday.day_end_date_time).format("YYYY-MM-DD HH:mm:ss");
        //As user records are sorted on the basis of shift start time so picked up the first one because it will be the earliest
        //If shift start time is null on user then pick default time from subscriber
        let shiftStartTime = userDataForRecordsHavingTimesheetsRecords[0].shift_start_time;
        if (!shiftStartTime)
            shiftStartTime = defaultShiftStartTime;
        //Fetch drive events records for given time range
        //Fetch timesheet records for given start date and end date
        let queryString = (0, db_1.getQueryToFetchDriveTimeEvents)(availableUserIdString, validQueryParams, range_day_end_date_time, accountId, shiftStartTime);
        driveEvents = yield postgres.query(queryString);
        //If no record found then throw error
    }
    catch (exception) {
        console.log(`Exception while fetching events records: ${exception}`);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    if (loggingEnabled)
        console.log(`Timesheet events records are fetched successfully`);
    /**************************************************************/
    let minimumShiftStartTime = '';
    let maxDayEndDateTime = '';
    let rows;
    let maxIntervalsOfRow = 0;
    try {
        let rowsUtility = (0, utilities_1.getRowsAndUtilitiesMapping)(workDayBreaks, workDayRecords, validQueryParams, driveEvents, isOverTimeEnabled, userDataForRecordsHavingTimesheetsRecords);
        rows = rowsUtility.rows;
        minimumShiftStartTime = rowsUtility.minimumShiftStartTime;
        maxDayEndDateTime = rowsUtility.maxDayEndDateTime;
        maxIntervalsOfRow = rowsUtility.maxIntervalsOfRow;
    }
    catch (exception) {
        console.log(`Exception while creating row mapping:${exception}`);
        console.trace(exception);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    /**************************************************************/
    let columns;
    try {
        columns = (0, utilities_1.getColumnsMapping)(minimumShiftStartTime, maxDayEndDateTime);
    }
    catch (exception) {
        console.log(`Exception while creating column mapping:${exception}`);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    /**************************************************************/
    // let excelBuffer
    try {
        yield createExcel_1.TimesheetExcel.saveExcelFileInLambdaTmp(columns, rows, validQueryParams, maxIntervalsOfRow, subscriberName);
    }
    catch (exception) {
        console.log(`Exception while creating excel buffer: ${exception}`);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    if (loggingEnabled)
        console.log(`Excel created successfully and saved successfully`);
    /**************************************************************/
    let base64StringOfTimeSheetPDF = "";
    try {
        base64StringOfTimeSheetPDF = yield createExcel_1.TimesheetExcel.getBase64OfPdfConvertedFromExcel();
    }
    catch (exception) {
        console.log(`Exception while converting excel file into pdf base64 string: ${exception}`);
        console.log(`User attribute from cognito: ${JSON.stringify(userAttribute, null, 4)}`);
        apiResponse = (0, handleException_1.getException)(exception.message, apiResponse, errorResponseBody);
        return apiResponse;
    }
    /**************************************************************/
    let filename = `${FILE_NAME}_${subscriberName}_${validQueryParams.date}.pdf`;
    apiResponse.statusCode = 200;
    apiResponse.body = base64StringOfTimeSheetPDF;
    apiResponse.isBase64Encoded = true;
    apiResponse.headers = {
        "Content-Type": "application/pdf",
        "Content-Disposition": `attachment; filename=\"${filename}\"`
    };
    return apiResponse;
});
exports.handler = handler;
//# sourceMappingURL=index.js.map