"use strict";
/******************************************************************************
 *
 * Copyright (c) 2019-2021 ServiceActivator Inc. All Rights Reserved.
 *
 * Unauthorized copying of the code in this file, completely or partially,
 * via any medium is strictly prohibited.
 *
 * Proprietary and Confidential.
 *
 * Written by 'Keshav Gupta' <keshav.gupta@wakencode.com>, February 2021
 *
 *****************************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
exports.getQueryToFetchDriveTimeEvents = exports.getQueryToFetchWorkDayRecords = exports.getQueryToFetchWorkDayBreaksRecords = exports.getQueryToFetchTimesheetConfigs = exports.getQueryToFetchTimesheetRecords = exports.getQueryToFetchSubscriberData = exports.getQueryToFetchUserData = void 0;
const utilities_1 = require("../utils/utilities");
/**************************************************************/
let loggingEnabled = (0, utilities_1.isLoggingEnabled)();
/**************************************************************/
/**
 * create query to fetch subscriber data
 *
 * @param {string} accountId account id of the user
 *  @param {ValidQueryParams} validQueryParams account id of the user
 * @returns {string} query string to fetch subscriber data
 */
function getQueryToFetchUserData(accountId) {
    //Create query to fetch user data
    let queryString = `Select id, first_name, last_name, shift_start_time from users where salesforce_account_id = '${accountId}' ORDER BY shift_start_time`;
    if (loggingEnabled)
        console.log(`Query to to fetch subscriber: ${queryString}`);
    return queryString;
}
exports.getQueryToFetchUserData = getQueryToFetchUserData;
/**************************************************************/
/**
 * create query to fetch subscriber data
 *
 * @param {string} accountId account id of the user
 * @returns {string} query string to fetch subscriber data
 */
function getQueryToFetchSubscriberData(accountId) {
    //Create query to fetch subscriber data
    let queryString = `SELECT is_timesheet_enabled, name, default_shift_start_time FROM subscribers WHERE id = '${accountId}'`;
    if (loggingEnabled)
        console.log(`Query to to fetch subscriber: ${queryString}`);
    return queryString;
}
exports.getQueryToFetchSubscriberData = getQueryToFetchSubscriberData;
/**************************************************************/
/**
 * create query to fetch timesheet records
 *
 * @param {string} userIdStr unique id's of user in csv formatted
 * @param {ValidQueryParams} validQueryParams valid query params in request
 * @returns {string} query string to fetch timesheet records
 */
function getQueryToFetchTimesheetRecords(userIdStr, validQueryParams) {
    //Create query to fetch timesheet records
    let queryString = `SELECT user_id FROM timesheets WHERE user_id IN (${userIdStr}) AND start_date <= '${validQueryParams.date}' AND end_date >='${validQueryParams.date}' AND type = 'PAYROLL_CYCLE'`;
    if (loggingEnabled)
        console.log(`Query to to fetch timesheet records: ${queryString}`);
    return queryString;
}
exports.getQueryToFetchTimesheetRecords = getQueryToFetchTimesheetRecords;
/**************************************************************/
/**
 * create query to fetch timesheet configs
 *
 * @param {string} accountId account id of the user
 * @returns {string} query string to fetch timesheet configs
 */
function getQueryToFetchTimesheetConfigs(accountId) {
    //Create query to fetch timesheet configs
    let queryString = `SELECT working_seconds_in_day, is_over_time_enabled FROM timesheet_configs WHERE subscriber_id = '${accountId}'`;
    if (loggingEnabled)
        console.log(`Query to to fetch timesheet configs: ${queryString}`);
    return queryString;
}
exports.getQueryToFetchTimesheetConfigs = getQueryToFetchTimesheetConfigs;
/**************************************************************/
/**
 * Return query to fetch workday breaks records
 *
 * @param {string} userIdStr unique id's of user in csv formatted
 * @param {ValidQueryParams} validQueryParams validated query params
 * @param {string} accountId unique Id of account
 * @returns {string} query to fetch workdays breaks from postgresql db
 */
function getQueryToFetchWorkDayBreaksRecords(userIdStr, validQueryParams, accountId) {
    //Create query to fetch workday breaks records
    let queryString = `SELECT * FROM work_day_breaks WHERE user_id IN (${userIdStr}) AND subscriber_id = '${accountId}' AND date = '${validQueryParams.date}' ORDER BY date ASC`;
    if (loggingEnabled)
        console.log(`Query to to fetch work day breaks records: ${queryString}`);
    return queryString;
}
exports.getQueryToFetchWorkDayBreaksRecords = getQueryToFetchWorkDayBreaksRecords;
/**************************************************************/
/**
 * Return query to fetch workday records
 *
 * @param {string} userIdStr unique id's of user in csv formatted
 * @param {ValidQueryParams} validQueryParams validated query params
 * @param {string} accountId unique Id of account
 * @returns query to fetch workdays from postgresql db
 */
function getQueryToFetchWorkDayRecords(userIdStr, validQueryParams, accountId) {
    //Create query to fetch workday records
    let queryString = `SELECT * FROM work_days WHERE user_id IN (${userIdStr}) AND subscriber_id = '${accountId}' AND date = '${validQueryParams.date}' ORDER BY date ASC`;
    if (loggingEnabled)
        console.log(`Query to to fetch work day records : ${queryString}`);
    return queryString;
}
exports.getQueryToFetchWorkDayRecords = getQueryToFetchWorkDayRecords;
/**************************************************************/
/**
 * Return query to fetch drive time events
 *
 * @param {string} userId unique Id of user
 * @param {ValidQueryParams} validQueryParams validated query params
 * @param {string} range_day_end_date_time Workday which has non nullable day_end_date_time and nearest to input endDate
 * @param {string} accountId unique Id of account
 * @returns query to fetch drive time events from workday events table in postgresql db
 */
function getQueryToFetchDriveTimeEvents(userIdStr, validQueryParams, rangeDayEndDateTime, accountId, shiftStartTime) {
    let queryString = `SELECT * FROM work_day_events WHERE user_id IN (${userIdStr}) AND subscriber_id = '${accountId}' AND timestamp >= '${validQueryParams.date} ${shiftStartTime}' AND timestamp <= '${rangeDayEndDateTime}' AND (event = 'DRIVE_START' OR event = 'DRIVE_END') ORDER BY timestamp`;
    if (loggingEnabled)
        console.log(`Query to to fetch work day events for drive_time : ${queryString}`);
    return queryString;
}
exports.getQueryToFetchDriveTimeEvents = getQueryToFetchDriveTimeEvents;
//# sourceMappingURL=db.js.map