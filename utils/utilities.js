"use strict";
/******************************************************************************
 *
 * Copyright (c) 2019-2021 ServiceActivator Inc. All Rights Reserved.
 *
 * Unauthorized copying of the code in this file, completely or partially,
 * via any medium is strictly prohibited.
 *
 * Proprietary and Confidential.
 *
 * Written by 'Keshav Gupta' <keshav.gupta@wakencode.com>, February 2021
 *
 *****************************************************************************/
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getNumberOfIntervalOfMinutes = exports.getRowsAndUtilitiesMapping = exports.getColumnsMapping = exports.getValidQueryParams = exports.isLoggingEnabled = void 0;
const lodash_1 = __importDefault(require("lodash"));
const moment_1 = __importDefault(require("moment"));
const moment_timezone_1 = __importDefault(require("moment-timezone"));
const constants_1 = require("./constants");
/**************************************************************/
//To suppress moment deprecation warnings
moment_1.default.suppressDeprecationWarnings = true;
/**************************************************************/
//Environment variable
const LOGGING_ENABLED = process.env.LOGGING_ENABLED || "";
const TIME_INTERVAL = process.env.TIME_INTERVAL || '';
const DRIVE_EVENTS_CUTOFF_SECONDS = process.env.DRIVE_EVENTS_CUTOFF_SECONDS || '';
const timeIntervalInt = parseInt(TIME_INTERVAL);
const driveEventsCutOffTimeInt = parseInt(DRIVE_EVENTS_CUTOFF_SECONDS);
/**************************************************************/
let loggingEnabled = false;
if (LOGGING_ENABLED == "true") {
    loggingEnabled = true;
}
/**************************************************************/
/**
 * Return true if logging is enabled
 */
function isLoggingEnabled() {
    return loggingEnabled;
}
exports.isLoggingEnabled = isLoggingEnabled;
/**************************************************************/
/**
 * Validate query parameters
 *
 * @param {APIGatewayProxyEventQueryStringParameters | null} queryParams query parameters in request
 * @returns {ValidQueryParams} valid query parameters
 */
function getValidQueryParams(queryParams) {
    let date = lodash_1.default.get(queryParams, "date", null);
    if (!date || !(0, moment_1.default)(date, 'YYYYMMDD', true).isValid())
        throw new Error(constants_1.ERROR_CODES.INVALID_DATE);
    return {
        date: moment_1.default.utc(date).format('YYYY-MM-DD')
    };
}
exports.getValidQueryParams = getValidQueryParams;
/**************************************************************/
/**
 * Get excel-js column mapping
 *
 * @param {string} minimumShiftStartDateTime Minimum shift start date time
 * @param {string} maxDayEndDateTime Maximum day end time date time among all users
 * @returns {Columns} Array of columns mapping to create columns in excel sheet
 */
function getColumnsMapping(minimumShiftStartDateTime, maxDayEndDateTime) {
    //No of interval in an hour for given minute time mentioned in env
    let interval = 60 / timeIntervalInt;
    //Initial index of loop
    let initialIndex = 60 / interval;
    // Hour distribution array
    let hourDistributionOfColumnArr = new Array();
    for (let i = 1; i < interval; i++) {
        hourDistributionOfColumnArr.push({
            name: `${Math.round(i * initialIndex)}`
        });
    }
    let columns = new Array();
    //Loop on minimum shift start date time till maximum workday end time with increment of an hour
    for (let m = (0, moment_1.default)(minimumShiftStartDateTime); m.isSameOrBefore(maxDayEndDateTime); m.add(1, 'h')) {
        let time = (0, moment_timezone_1.default)(m).tz('America/Los_Angeles').format('h A');
        columns.push({
            name: `${time}`
        }, ...hourDistributionOfColumnArr);
    }
    //Push at start of the array
    columns.unshift({
        name: 'Employee'
    }, {
        name: 'Delete'
    }, {
        name: 'Delete'
    }, {
        name: 'Delete'
    }, {
        name: 'Delete'
    }, {
        name: 'Delete'
    }, {
        name: 'Delete'
    }, {
        name: 'Delete'
    }, {
        name: 'Delete'
    }, {
        name: 'Delete'
    }, {
        name: 'Delete'
    }, {
        name: 'Delete'
    });
    //'Delete' columns will be deleted later after creating excel sheet
    let hoursSummaryOfWorkDayArr = new Array(40);
    hoursSummaryOfWorkDayArr.fill({
        name: 'Delete'
    });
    //Push temp array at end for regular time, overtime & total hours summary 
    columns.push(...hoursSummaryOfWorkDayArr);
    return columns;
}
exports.getColumnsMapping = getColumnsMapping;
/**************************************************************/
/**
 *
 * @param workDayBreaks
 * @param workDay
 * @param isOverTimeEnabled
 * @returns object containing 2D array of rows, shiftStartTime & maxDayEndDateTime
 */
function getRowsAndUtilitiesMapping(workDayBreaks, workDay, validQueryParams, driveEvents, isOverTimeEnabled, userDataForRecordsHavingTimesheetsRecords) {
    //Each workday map structure
    let eventsMapStructure = new Map();
    let workDayArr = new Array();
    //This array will be used to get Max value from all the day_end_date_time
    //Contains WorkDayRecords["day_end_date_time"] moment object array
    let workdayEndDateTimeArr = new Array();
    //This array will be used to get Max value from all the shift_start_timestamp
    //Contains WorkDayRecords["shift_start_timestamp"] moment object array
    let shiftStartTimeStampArr = new Array();
    //Loop on workday array
    for (let i = 0; i < workDay.length; i++) {
        let userId = workDay[i].user_id;
        // Skip workday if day_end_date_time is null
        if (workDay[i].day_end_date_time == null) {
            continue;
        }
        shiftStartTimeStampArr.push((0, moment_1.default)((0, moment_1.default)(workDay[i].shift_start_timestamp).format("2000-12-12 HH:mm:ss")));
        for (let eachClockIn of workDay[i].shift_time_entries) {
            //If clockOutDateTime is null then skip that entry
            if (eachClockIn.clockOutDateTime == null) {
                continue;
            }
            //copy by value
            let eachWorkDayJSON = Object.assign({}, workDay[i]);
            //Break events
            eachWorkDayJSON.breaks = [];
            //Drive events
            eachWorkDayJSON.drive = [];
            eachWorkDayJSON.day_start_date_time = eachClockIn.clockInDateTime;
            eachWorkDayJSON.day_end_date_time = eachClockIn.clockOutDateTime;
            //Iterate work day breaks records
            let workdayShiftClockInDateTime = (0, moment_1.default)(eachClockIn.clockInDateTime);
            let workdayShiftClockOutDateTime = (0, moment_1.default)(eachClockIn.clockOutDateTime);
            workDayBreaks.forEach((eachBreak, index) => {
                //If events is already iterated then mark it as true so that event won't be processed again
                if (!workDayBreaks[index].is_iterated) {
                    let breakStartTimestamp = (0, moment_1.default)(eachBreak.start_time);
                    //If break event falls between clockIn & clockOut time
                    if (eachBreak.user_id == userId && (0, moment_1.default)(breakStartTimestamp).isBetween(workdayShiftClockInDateTime, workdayShiftClockOutDateTime)) {
                        eachWorkDayJSON.breaks.push(eachBreak);
                        //Mark that event as true so that it won't be iterated in any iteration
                        workDayBreaks[index].is_iterated = true;
                    }
                }
            });
            let driveStartWorkdayArr = new Array();
            driveEvents.forEach((eachDriveEvent) => {
                //If events is already iterated then mark it as true so that event won't be processed again
                let driveTimestamp = (0, moment_1.default)(eachDriveEvent.timestamp);
                //If drive event is of range between user clockIn and clockOut
                if (eachDriveEvent.user_id == userId && driveTimestamp.isBetween(workdayShiftClockInDateTime, workdayShiftClockOutDateTime)) {
                    //Push the events having DRIVE_START event
                    if (eachDriveEvent.event == 'DRIVE_START') {
                        //Add index which is required later in code to mark that index as is_iterated so we don't have to iterate it again
                        driveStartWorkdayArr.push(eachDriveEvent);
                    }
                    //If it is a drive end event before that we already have a drive start in driveStartWorkdayArr
                    if (eachDriveEvent.event == 'DRIVE_END' && driveStartWorkdayArr.length > 0) {
                        //Find nearest DRIVE_START wrt to given DRIVE_END
                        let driveStartWorkday = lodash_1.default.maxBy(driveStartWorkdayArr, (item) => {
                            return item.timestamp;
                        });
                        let driveDuration = moment_1.default.duration(driveTimestamp.diff((0, moment_1.default)(driveStartWorkday.timestamp)));
                        if (driveDuration.asSeconds() > driveEventsCutOffTimeInt) {
                            eachWorkDayJSON.drive.push({
                                start_time: driveStartWorkday.timestamp,
                                end_time: eachDriveEvent.timestamp
                            });
                            driveStartWorkdayArr = [];
                        }
                    }
                }
            });
            workDayArr.push(eachWorkDayJSON);
        }
        eventsMapStructure.set(workDay[i].user_id, {
            workDayArr: workDayArr,
            //get Max day_end_date_time in a workday irrespective of multiple clock-in & clock-out
            maxDayEndTime: workDay[i].day_end_date_time
        });
        let workdayEndDateTime = '';
        let workdayStartDateTime = '';
        let workdayEndDateTimeMoment = (0, moment_1.default)(workDay[i].day_end_date_time);
        let workdayStartDateTimeMoment = (0, moment_1.default)(workDay[i].day_start_date_time);
        //If workday start date is more than shift start date  then add a day in that date
        if (workdayStartDateTimeMoment.isAfter(workDay[i].shift_start_timestamp, 'day')) {
            workdayEndDateTime = workdayEndDateTimeMoment.format('2000-12-13 HH:mm:ss');
            workdayStartDateTime = workdayStartDateTimeMoment.format('2000-12-13 HH:mm:ss');
        }
        else {
            workdayEndDateTime = workdayEndDateTimeMoment.format('2000-12-12 HH:mm:ss');
            workdayStartDateTime = workdayStartDateTimeMoment.format('2000-12-12 HH:mm:ss');
        }
        if ((0, moment_1.default)(workdayStartDateTime).isSameOrAfter((0, moment_1.default)(workdayEndDateTime))) {
            workdayEndDateTime = (0, moment_1.default)(workdayEndDateTime).add(1, 'd').format("YYYY-MM-DD HH:mm:ss");
        }
        //push moment object of work day end
        workdayEndDateTimeArr.push((0, moment_1.default)(workdayEndDateTime));
        //get Max day_end_date_time in a workday of day
        //Assigning empty array to variable for fresh iteration
        workDayArr = [];
    }
    //Get max day-end time for the given time period provided in query params
    let maxWorkdayEndDateTime = moment_1.default.max(workdayEndDateTimeArr).format("YYYY-MM-DD hh:mm:ss A");
    //Get minimum shift start time for the given time period provided in query params
    let minimumShiftStartDateTime = moment_1.default.min(shiftStartTimeStampArr).format("YYYY-MM-DD hh:mm:ss A");
    //Each workday map structure
    let eventsFinalMapStructure = new Map();
    let maxIntervalsOfRow = getNumberOfIntervalOfMinutes(minimumShiftStartDateTime, maxWorkdayEndDateTime);
    let maxIntervalsOfRowIncludingLastHourCompleteInterval = maxIntervalsOfRow + maxIntervalsOfRow % (60 / timeIntervalInt);
    //Replace userId with name of the user in map structure
    userDataForRecordsHavingTimesheetsRecords.map(eachUser => {
        if (eventsMapStructure.has(eachUser.id)) {
            let value = eventsMapStructure.get(eachUser.id);
            if (value) {
                eventsFinalMapStructure.set(`${eachUser.first_name} ${eachUser.last_name}`, value);
            }
        }
        else {
            //Add those users which has time configs but don't have the workday for given date
            eventsFinalMapStructure.set(`${eachUser.first_name} ${eachUser.last_name}`, { workDayArr: [], maxDayEndTime: 'NO_WORK_DAY' });
        }
    });
    let finalsRowsArr = new Array();
    //24 hour standard row
    let hourStandardRow = new Array();
    for (let i = 1; i <= 24; i++) {
        let perCellInHour = 60 / timeIntervalInt;
        let arr = new Array(perCellInHour);
        //extra space
        arr.fill(' ');
        arr[0] = `H${i}`;
        hourStandardRow.push(...arr);
    }
    //Side row
    let sideHoursSummaryRow = new Array(30);
    sideHoursSummaryRow.fill('');
    //Iterate on the map structure for each date
    eventsFinalMapStructure.forEach((eachDateEvents, name) => {
        let minimumShiftStartDateTimeForEachEvent = validQueryParams.date + ' ' + minimumShiftStartDateTime.slice(11);
        //Adding set of interval for total & overtime summary (one extra hour) in maxIntervalsOfRow
        let eachWorkdayRow = new Array(maxIntervalsOfRow + 60 / timeIntervalInt);
        //Default empty string in all workday rows
        eachWorkdayRow.fill('');
        let eachWorkDayHourRow;
        if (eachDateEvents.maxDayEndTime == 'NO_WORK_DAY') {
            eachWorkDayHourRow = new Array(maxIntervalsOfRowIncludingLastHourCompleteInterval);
        }
        else {
            eachWorkDayHourRow = new Array(getNumberOfIntervalOfMinutes(minimumShiftStartDateTimeForEachEvent, eachDateEvents.maxDayEndTime));
        } //Default empty string in all workday hour rows
        eachWorkDayHourRow.fill('');
        //Get workDay array
        let workDayArr = eachDateEvents.workDayArr;
        // To pass by value
        let eachWorkDayHourArr = hourStandardRow.slice(0);
        //This will help in determining in multiple clock-in & clock out scenario if overtime has started in previous clock-in
        let isOvertimeStarted = false;
        for (let workDay of workDayArr) {
            let intervalOfWorkday = getNumberOfIntervalOfMinutes(workDay.day_start_date_time, workDay.day_end_date_time);
            //Get work day row summary of workday range
            let workDayRowsMappingArr = new Array(intervalOfWorkday);
            let tempHourStandardRow = hourStandardRow.slice(0);
            workDayRowsMappingArr.fill('W');
            //Unpaid breaks [Working hours won't be included in unpaid breaks]
            let regularTimeUnpaidBreaks = 0;
            let overTimeUnpaidBreaks = 0;
            //process drive events
            workDay.drive.forEach((eachDrive) => {
                let indexOfStartIntervalOfDriveBreak = getNumberOfIntervalOfMinutes(workDay.day_start_date_time, eachDrive.start_time);
                let driveIntervalLength = getNumberOfIntervalOfMinutes(eachDrive.start_time, eachDrive.end_time);
                workDayRowsMappingArr.fill('D', indexOfStartIntervalOfDriveBreak, indexOfStartIntervalOfDriveBreak + driveIntervalLength);
                let driveInterval = new Array(driveIntervalLength);
                driveInterval.fill('');
            });
            //Iterate each break in workday and map that in workDayRowsMappingArr 
            workDay.breaks.forEach((eachBreaks) => {
                //Lunch break event
                if (eachBreaks.type == 'MEAL') {
                    let indexOfStartIntervalOfMealBreak = getNumberOfIntervalOfMinutes(workDay.day_start_date_time, eachBreaks.start_time);
                    let breakIntervalLength = getNumberOfIntervalOfMinutes(eachBreaks.start_time, eachBreaks.end_time);
                    workDayRowsMappingArr.fill('L', indexOfStartIntervalOfMealBreak, indexOfStartIntervalOfMealBreak + breakIntervalLength);
                    let breakInterval = new Array(breakIntervalLength);
                    breakInterval.fill('');
                    //Shift hours of meal break for the unpaid break duration
                    eachWorkDayHourArr.splice(indexOfStartIntervalOfMealBreak, 0, ...breakInterval);
                    // Meal break is unpaid break
                    if (eachBreaks.interval == 'SECOND') {
                        regularTimeUnpaidBreaks += breakIntervalLength;
                    }
                    else if (eachBreaks.interval == 'FOURTH') {
                        overTimeUnpaidBreaks += breakIntervalLength;
                    }
                }
                else if (eachBreaks.type == 'REST') {
                    //Rest breaks can be paid as well as unpaid
                    let indexOfStartIntervalOfRestBreak = getNumberOfIntervalOfMinutes(workDay.day_start_date_time, eachBreaks.start_time);
                    let breakStartTime = (0, moment_1.default)(eachBreaks.start_time).format("HH:mm:ss");
                    let breakEndTime = (0, moment_1.default)(eachBreaks.end_time).format("HH:mm:ss");
                    let breakDuration = moment_1.default.duration(((0, moment_1.default)(breakEndTime, "HH:mm:ss")).diff((0, moment_1.default)(breakStartTime, "HH:mm:ss")));
                    // Convert moment duration in breaks
                    let breakMinutes = breakDuration.asMinutes();
                    // A perfect hour is made up
                    let remainderOfNextBreak = breakMinutes % timeIntervalInt;
                    let breaksIntervalLengthIndex = Math.ceil(breakMinutes / timeIntervalInt);
                    //If next break's left over break is less than 20% of interval then that break time will be ignored
                    if (remainderOfNextBreak < 0.2 * timeIntervalInt) {
                        breaksIntervalLengthIndex = breaksIntervalLengthIndex - 1;
                    }
                    let paidRestIntervalsLength = 0;
                    let unpaidRestIntervalsLength = 0;
                    //If break is paid
                    if (eachBreaks.paid) {
                        //allowance in minutes.. if not paid then allowance is 0
                        let allowance = eachBreaks.allowance_seconds / 60;
                        paidRestIntervalsLength = Math.ceil(allowance / timeIntervalInt);
                        unpaidRestIntervalsLength = breaksIntervalLengthIndex - paidRestIntervalsLength;
                        //paid is denoted by 'B'
                        workDayRowsMappingArr.fill('B', indexOfStartIntervalOfRestBreak, indexOfStartIntervalOfRestBreak + paidRestIntervalsLength);
                        //Unpaid is denoted by 'B ' ( notice the space at the end of string)
                        workDayRowsMappingArr.fill('B ', indexOfStartIntervalOfRestBreak + paidRestIntervalsLength, indexOfStartIntervalOfRestBreak + breaksIntervalLengthIndex);
                        //Add breaks
                        let breakInterval = new Array(unpaidRestIntervalsLength);
                        breakInterval.fill('');
                        if (eachBreaks.interval == 'FIRST' || eachBreaks.interval == 'SECOND' || eachBreaks.interval == 'THIRD') {
                            regularTimeUnpaidBreaks += unpaidRestIntervalsLength;
                        }
                        else if (eachBreaks.interval == 'FOURTH' || eachBreaks.interval == 'FIFTH' || eachBreaks.interval == 'SIXTH') {
                            overTimeUnpaidBreaks += unpaidRestIntervalsLength;
                        }
                        eachWorkDayHourArr.splice(indexOfStartIntervalOfRestBreak + paidRestIntervalsLength, 0, ...breakInterval);
                    }
                    else {
                        unpaidRestIntervalsLength = breaksIntervalLengthIndex;
                        //paid 'B'
                        workDayRowsMappingArr.fill('B ', indexOfStartIntervalOfRestBreak, indexOfStartIntervalOfRestBreak + unpaidRestIntervalsLength);
                        //Add breaks
                        let breakInterval = new Array(unpaidRestIntervalsLength);
                        breakInterval.fill('');
                        if (eachBreaks.interval == 'FIRST' || eachBreaks.interval == 'SECOND' || eachBreaks.interval == 'THIRD') {
                            //Assumption is allowance_start_time will always have timestamp of 6th hour (startTime of third interval)
                            if (unpaidRestIntervalsLength != 0 && eachBreaks.interval == 'THIRD' && moment_1.default.duration((0, moment_1.default)(eachBreaks.start_time).diff(eachBreaks.allowance_start_time)).asHours() > 2) {
                                overTimeUnpaidBreaks += unpaidRestIntervalsLength;
                            }
                            else {
                                regularTimeUnpaidBreaks += unpaidRestIntervalsLength;
                            }
                        }
                        else if (eachBreaks.interval == 'FOURTH' || eachBreaks.interval == 'FIFTH' || eachBreaks.interval == 'SIXTH') {
                            overTimeUnpaidBreaks = overTimeUnpaidBreaks + unpaidRestIntervalsLength;
                        }
                        eachWorkDayHourArr.splice(indexOfStartIntervalOfRestBreak, 0, ...breakInterval);
                    }
                }
            });
            eachWorkDayHourArr = eachWorkDayHourArr.slice(0, intervalOfWorkday);
            //If overtime is enabled then charge them in hours
            if (isOverTimeEnabled) {
                //convert seconds into hour and find number of time intervals
                let workingTimeIncludingBreaksInterval = 0;
                //If isOvertimeStarted is true that means previous clock-in was already in overtime so this will be in overtime
                if (!isOvertimeStarted)
                    workingTimeIncludingBreaksInterval = workDay.regular_time_seconds / (60 * timeIntervalInt) + regularTimeUnpaidBreaks;
                if (eachWorkDayHourArr.length > workingTimeIncludingBreaksInterval && workDay.over_time_seconds > 0) {
                    let overtimeArr = eachWorkDayHourArr.splice(workingTimeIncludingBreaksInterval, eachWorkDayHourArr.length);
                    //Add extra space at the start of each element of an array if it's not an empty string because it denotes empty space
                    let finalOvertimeArr = overtimeArr.map((eachInterval) => {
                        return eachInterval == '' ? eachInterval : ` ${eachInterval}`;
                    });
                    eachWorkDayHourArr.splice(workingTimeIncludingBreaksInterval, finalOvertimeArr.length, ...finalOvertimeArr);
                    //If we are here then we are already in overtime hence mark it as true
                    isOvertimeStarted = true;
                }
            }
            let indexOfWorkdayStart = getNumberOfIntervalOfMinutes(minimumShiftStartDateTimeForEachEvent, workDay.day_start_date_time);
            eachWorkdayRow.splice(indexOfWorkdayStart, workDayRowsMappingArr.length, ...workDayRowsMappingArr);
            eachWorkDayHourRow.splice(indexOfWorkdayStart, eachWorkDayHourArr.length, ...eachWorkDayHourArr);
            eachWorkDayHourArr = tempHourStandardRow.slice(eachWorkDayHourArr.length - regularTimeUnpaidBreaks - overTimeUnpaidBreaks);
        }
        if (workDayArr.length > 0) {
            let overtimeBreaksSecond = 0;
            let breakPaidSeconds = workDayArr[0].break_paid_seconds;
            //FOURTH interval has provision of a paid break so if it is more than 1200 seconds ie 20 minutes then that break will be counted in paid break
            if (breakPaidSeconds > 1200) {
                overtimeBreaksSecond = 1200 - breakPaidSeconds;
            }
            sideHoursSummaryRow[2] = `Regular - Work - ${Math.round((workDayArr[0].regular_time_seconds + breakPaidSeconds - workDayArr[0].driving_seconds) * 10 / 3600) / 10}\n  \t \t Drive - ${Math.round(workDayArr[0].driving_seconds * 10 / 3600) / 10}`;
            sideHoursSummaryRow[10] = `Overtime - Work - ${Math.round((workDayArr[0].over_time_seconds + overtimeBreaksSecond) * 10 / 3600) / 10}`;
            sideHoursSummaryRow[20] = `Total - ${Math.round((workDayArr[0].total_working_seconds + breakPaidSeconds) * 10 / 3600) / 10}`;
            eachWorkdayRow.push(...sideHoursSummaryRow);
        }
        // Add date at the start of every row
        eachWorkdayRow.unshift(`${name}`, '', '', '', '', '', '', '', '', '', '', '');
        eachWorkDayHourRow.unshift('', '', '', '', '', '', '', '', '', '', '', '');
        finalsRowsArr.push(eachWorkdayRow);
        finalsRowsArr.push(eachWorkDayHourRow);
    });
    return {
        rows: finalsRowsArr,
        minimumShiftStartTime: minimumShiftStartDateTime,
        maxDayEndDateTime: maxWorkdayEndDateTime,
        //max interval of row including complete end hour interval
        maxIntervalsOfRow: maxIntervalsOfRowIncludingLastHourCompleteInterval
    };
}
exports.getRowsAndUtilitiesMapping = getRowsAndUtilitiesMapping;
/**************************************************************/
/**
 *
 * @param startDateTime
 * @param endDateTime
 * @returns each cell interval in minutes
 */
function getNumberOfIntervalOfMinutes(startDateTime, endDateTime) {
    let startDateTimeMoment = (0, moment_1.default)(startDateTime);
    let endDateTimeMoment = (0, moment_1.default)(endDateTime);
    // If startDateTimeMoment is before endDateTimeMoment then it's of next day so add a day in endDateTimeMoment
    if (startDateTimeMoment.isSameOrAfter(endDateTimeMoment)) {
        endDateTimeMoment = endDateTimeMoment.add(1, 'd');
    }
    var duration = moment_1.default.duration((endDateTimeMoment).diff(startDateTimeMoment));
    let minutes = duration.asMinutes();
    //Return no of intervals
    return Math.ceil(minutes / timeIntervalInt);
}
exports.getNumberOfIntervalOfMinutes = getNumberOfIntervalOfMinutes;
//# sourceMappingURL=utilities.js.map