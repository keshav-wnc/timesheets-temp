"use strict";
/******************************************************************************
 *
 * Copyright (c) 2019-2021 ServiceActivator Inc. All Rights Reserved.
 *
 * Unauthorized copying of the code in this file, completely or partially,
 * via any medium is strictly prohibited.
 *
 * Proprietary and Confidential.
 *
 * Written by 'Keshav Gupta' <keshav.gupta@wakencode.com>, February 2021
 *
 *****************************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
exports.getException = void 0;
//Required dependencies
const constants_1 = require("./constants");
/**************************************************************/
/**
 * Return api response with error code and description
 *
 * @param {string} message
 * @param {ApiResponse} apiResponse
 * @param {ErrorResponseBody} errorResponseBody
 * @returns {ApiResponse} api response with error code and description
 */
function getException(message, apiResponse, errorResponseBody) {
    switch (message) {
        case constants_1.ERROR_CODES.UNAUTHORIZED:
            apiResponse.statusCode = 401;
            return apiResponse;
        case constants_1.ERROR_CODES.TIMESHEET_NOT_ENABLED:
            apiResponse.statusCode = 400;
            errorResponseBody.errorCode = 628;
            errorResponseBody.errorDescription = constants_1.ERROR_CODES.TIMESHEET_NOT_ENABLED;
            //Convert the body to String from JSON for API Gateway to parse further
            apiResponse.body = JSON.stringify(errorResponseBody);
            return apiResponse;
        case constants_1.ERROR_CODES.TIMESHEET_RECORDS_NOT_FOUND:
            apiResponse.statusCode = 400;
            errorResponseBody.errorCode = 885;
            errorResponseBody.errorDescription = constants_1.ERROR_CODES.TIMESHEET_RECORDS_NOT_FOUND;
            //Convert the body to String from JSON for API Gateway to parse further
            apiResponse.body = JSON.stringify(errorResponseBody);
            return apiResponse;
        case constants_1.ERROR_CODES.INVALID_DATE:
            apiResponse.statusCode = 400;
            errorResponseBody.errorCode = 926;
            errorResponseBody.errorDescription = constants_1.ERROR_CODES.INVALID_DATE;
            //Convert the body to String from JSON for API Gateway to parse further
            apiResponse.body = JSON.stringify(errorResponseBody);
            return apiResponse;
        case constants_1.ERROR_CODES.TIMESHEET_DATA_NOT_FOUND:
            apiResponse.statusCode = 400;
            errorResponseBody.errorCode = 630;
            errorResponseBody.errorDescription = constants_1.ERROR_CODES.TIMESHEET_DATA_NOT_FOUND;
            //Convert the body to String from JSON for API Gateway to parse further
            apiResponse.body = JSON.stringify(errorResponseBody);
            return apiResponse;
        case constants_1.ERROR_CODES.TIMESHEET_CONFIGS_NOT_FOUND:
            apiResponse.statusCode = 400;
            errorResponseBody.errorCode = 631;
            errorResponseBody.errorDescription = constants_1.ERROR_CODES.TIMESHEET_CONFIGS_NOT_FOUND;
            //Convert the body to String from JSON for API Gateway to parse further
            apiResponse.body = JSON.stringify(errorResponseBody);
            return apiResponse;
        case constants_1.ERROR_CODES.NO_USERS_FOUND:
            apiResponse.statusCode = 400;
            errorResponseBody.errorCode = 651;
            errorResponseBody.errorDescription = constants_1.ERROR_CODES.NO_USERS_FOUND;
            //Convert the body to String from JSON for API Gateway to parse further
            apiResponse.body = JSON.stringify(errorResponseBody);
            return apiResponse;
        case constants_1.ERROR_CODES.INVALID_WORKDAYS_FOR_GIVEN_RANGE:
            apiResponse.statusCode = 400;
            errorResponseBody.errorCode = 653;
            errorResponseBody.errorDescription = constants_1.ERROR_CODES.INVALID_WORKDAYS_FOR_GIVEN_RANGE;
            //Convert the body to String from JSON for API Gateway to parse further
            apiResponse.body = JSON.stringify(errorResponseBody);
            return apiResponse;
        default:
            apiResponse.statusCode = 500;
            return apiResponse;
    }
}
exports.getException = getException;
//# sourceMappingURL=handleException.js.map