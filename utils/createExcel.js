"use strict";
/******************************************************************************
 *
 * Copyright (c) 2019-2021 ServiceActivator Inc. All Rights Reserved.
 *
 * Unauthorized copying of the code in this file, completely or partially,
 * via any medium is strictly prohibited.
 *
 * Proprietary and Confidential.
 *
 * Written by 'Keshav Gupta' <keshav.gupta@wakencode.com>, February 2021
 *
 *****************************************************************************/
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimesheetExcel = void 0;
const exceljs_1 = __importDefault(require("exceljs"));
const moment_1 = __importDefault(require("moment"));
const path = require('path');
const fs = require('fs');
const lambdafs = require('lambdafs');
const { execSync } = require('child_process');
/**************************************************************/
//Env variables
const DRIVE_TIME_EVENT_COLOR_CODE = process.env.DRIVE_TIME_EVENT_COLOR_CODE || "";
const BACKGROUND_COLOR_CODE = process.env.BACKGROUND_COLOR_CODE || "";
const PAID_BREAK_EVENT_COLOR_CODE = process.env.PAID_BREAK_EVENT_COLOR_CODE || "";
const WORK_EVENT_COLOR_CODE = process.env.WORK_EVENT_COLOR_CODE || "";
const LUNCH_EVENT_COLOR_CODE = process.env.LUNCH_EVENT_COLOR_CODE || "";
const UNPAID_BREAK_EVENT_COLOR_CODE = process.env.UNPAID_BREAK_EVENT_COLOR_CODE || "";
const REGULAR_TIME_COLOR_CODE = process.env.REGULAR_TIME_COLOR_CODE || "";
const OVER_TIME_COLOR_CODE = process.env.OVER_TIME_COLOR_CODE || "";
const CELL_GRID_COLOR_CODE = process.env.CELL_GRID_COLOR_CODE || "";
/**************************************************************/
class TimesheetExcel {
    /**
     * Generate excel & save it in lambda tmp folder
     *
     * @param {Columns} columns Columns mapping in excel
     * @param {Rows} rows Rows mapping in excel
     * @param {ValidQueryParams} validQueryParams Validated query params
     * @param {number} maxIntervalsOfRow Maximum intervals
     * @param {string} subscriberName Subscriber name
     */
    static saveExcelFileInLambdaTmp(columns, rows, validQueryParams, maxIntervalsOfRow, subscriberName) {
        return __awaiter(this, void 0, void 0, function* () {
            //Initialize a workbook
            const workbook = new exceljs_1.default.Workbook();
            //Create a sheet
            const sheet = workbook.addWorksheet('TimeSheet', {
                pageSetup: {
                    paperSize: 3,
                    orientation: 'landscape',
                    fitToPage: true,
                }
            });
            //Time sheet summary
            sheet.addTable({
                name: 'Time_Summary',
                ref: 'A7',
                columns: columns,
                rows: rows,
            });
            //Default cell width
            sheet.properties.defaultColWidth = 2.5;
            sheet.eachRow((row, rowNumber) => {
                //Temporary variables
                let regularTimeMergedCellsStr = "";
                let overtimeMergedCellsStr = "";
                let totalTimeMergedCellsStr = "";
                //Adding time summary after row 7
                //Header column is row 7
                if (rowNumber == 7) {
                    let head = row;
                    head.alignment = {
                        vertical: 'top',
                        horizontal: 'left',
                        wrapText: false,
                        textRotation: 90,
                        shrinkToFit: true
                    };
                    head.eachCell((cell) => {
                        if (cell.value == 'Employee') {
                            cell.font = { bold: true };
                            cell.alignment = {
                                vertical: 'bottom',
                                horizontal: 'left',
                                textRotation: 0
                            };
                        }
                        // AM or PM
                        if (typeof (cell.value) == 'string' && (cell.value).slice(-1) == 'M') {
                            cell.font = { bold: true };
                        }
                        if (cell.value == 'Delete') {
                            cell.value = '';
                        }
                    });
                    head.height = 40;
                }
                else {
                    let rowsCounter = 0;
                    let hoursCounter = 0;
                    //Other columns
                    row.eachCell((cell) => {
                        cell.alignment = {
                            vertical: 'middle', horizontal: 'center'
                        };
                        //Even row of time events like 'W', 'B' , 'L'
                        if (rowNumber % 2 == 0) {
                            //index starting from 11 because first cell is of employee name which could be as long as 11 cells
                            if (rowsCounter > 11 && rowsCounter <= maxIntervalsOfRow + 11) {
                                cell.style.border = {
                                    top: {
                                        style: 'thin',
                                        color: { argb: CELL_GRID_COLOR_CODE }
                                    },
                                    left: {
                                        style: 'thin',
                                        color: { argb: CELL_GRID_COLOR_CODE }
                                    },
                                    bottom: {
                                        style: 'thin',
                                        color: { argb: CELL_GRID_COLOR_CODE }
                                    },
                                    right: {
                                        style: 'thin',
                                        color: { argb: CELL_GRID_COLOR_CODE }
                                    }
                                };
                            }
                            rowsCounter++;
                            let cellColour = BACKGROUND_COLOR_CODE; //"FFFFFF";
                            if (cell.value) {
                                if (typeof (cell.value) == 'string' && (cell.address).slice(0, 1) == 'A') {
                                    cell.alignment = {
                                        vertical: 'bottom',
                                        horizontal: 'left',
                                        textRotation: 0
                                    };
                                }
                                //Drive time events
                                if (cell.value == 'D') {
                                    cellColour = DRIVE_TIME_EVENT_COLOR_CODE;
                                } //Paid breaks events 
                                else if (cell.value == 'B') {
                                    cellColour = PAID_BREAK_EVENT_COLOR_CODE;
                                } //Work events
                                else if (cell.value == 'W') {
                                    cellColour = WORK_EVENT_COLOR_CODE;
                                } // Lunch events 
                                else if (cell.value == 'L') {
                                    cellColour = LUNCH_EVENT_COLOR_CODE;
                                } //Unpaid breaks 
                                else if (cell.value == 'B ') {
                                    cellColour = UNPAID_BREAK_EVENT_COLOR_CODE;
                                } //Regular time summary cells 
                                else if (typeof (cell.value) == 'string' && (cell.value).slice(0, 7) == 'Regular') {
                                    let col = parseInt(cell.col);
                                    if (regularTimeMergedCellsStr == '')
                                        sheet.mergeCells(rowNumber, col, rowNumber + 1, col + 6);
                                    regularTimeMergedCellsStr = 'Done';
                                    cell.style.border = {
                                        top: {
                                            style: 'thin'
                                        },
                                        left: {
                                            style: 'thin'
                                        },
                                        bottom: {
                                            style: 'thin'
                                        },
                                        right: {
                                            style: 'thin'
                                        }
                                    };
                                    cellColour = REGULAR_TIME_COLOR_CODE;
                                } //Over time summary cells  
                                else if (typeof (cell.value) == 'string' && (cell.value).slice(0, 8) == 'Overtime') {
                                    let col = parseInt(cell.col);
                                    if (overtimeMergedCellsStr == '')
                                        sheet.mergeCells(rowNumber, col, rowNumber, col + 8);
                                    overtimeMergedCellsStr = 'Done';
                                    cell.style.border = {
                                        top: {
                                            style: 'thin'
                                        },
                                        left: {
                                            style: 'thin'
                                        },
                                        bottom: {
                                            style: 'thin'
                                        },
                                        right: {
                                            style: 'thin'
                                        }
                                    };
                                    cellColour = OVER_TIME_COLOR_CODE;
                                } //Total time summary cells 
                                else if (typeof (cell.value) == 'string' && (cell.value).slice(0, 5) == 'Total') {
                                    let col = parseInt(cell.col);
                                    if (totalTimeMergedCellsStr == '')
                                        sheet.mergeCells(rowNumber, col, rowNumber, col + 6);
                                    totalTimeMergedCellsStr = 'Done';
                                    cell.style.border = {
                                        top: {
                                            style: 'thin'
                                        },
                                        left: {
                                            style: 'thin'
                                        },
                                        bottom: {
                                            style: 'thin'
                                        },
                                        right: {
                                            style: 'thin'
                                        }
                                    };
                                }
                                cell.fill = {
                                    type: 'pattern',
                                    pattern: 'solid',
                                    fgColor: {
                                        argb: cellColour
                                    }
                                };
                            }
                        }
                        else {
                            if (hoursCounter > 11 && hoursCounter <= maxIntervalsOfRow + 11) {
                                cell.style.border = {
                                    top: {
                                        style: 'thin',
                                        color: { argb: CELL_GRID_COLOR_CODE }
                                    },
                                    left: {
                                        style: 'thin',
                                        color: { argb: CELL_GRID_COLOR_CODE }
                                    },
                                    bottom: {
                                        style: 'thin',
                                        color: { argb: CELL_GRID_COLOR_CODE }
                                    },
                                    right: {
                                        style: 'thin',
                                        color: { argb: CELL_GRID_COLOR_CODE }
                                    }
                                };
                            }
                            hoursCounter++;
                            //Hour rows [odd number iterations]
                            let cellColour = UNPAID_BREAK_EVENT_COLOR_CODE;
                            //Regular hours 'H'
                            if (typeof (cell.value) == 'string' && ((cell.value).slice(0, 1) == 'H' || cell.value == ' ')) {
                                if ((cell.value).slice(0, 1) == 'H') {
                                    cell.style.border = {
                                        left: {
                                            style: 'thin'
                                        },
                                    };
                                }
                                cellColour = REGULAR_TIME_COLOR_CODE;
                            } // Overtime hours ' H'
                            else if (typeof (cell.value) == 'string' && ((cell.value).slice(0, 2) == ' H' || cell.value == '  ')) {
                                if ((cell.value).slice(0, 2) == ' H') {
                                    cell.style.border = {
                                        left: {
                                            style: 'thin'
                                        },
                                    };
                                }
                                cellColour = OVER_TIME_COLOR_CODE;
                            } // Regular time summary cells falling in hours row section 
                            else if (typeof (cell.value) == 'string' && (cell.value).slice(0, 7) == 'Regular') {
                                // Multiple rows and column merged for drive time time summary
                                cell.style.border = {
                                    top: {
                                        style: 'thin'
                                    },
                                    left: {
                                        style: 'thin'
                                    },
                                    bottom: {
                                        style: 'thin'
                                    },
                                    right: {
                                        style: 'thin'
                                    }
                                };
                                cellColour = REGULAR_TIME_COLOR_CODE;
                            }
                            cell.fill = {
                                type: 'pattern',
                                pattern: 'solid',
                                fgColor: {
                                    argb: cellColour
                                }
                            };
                        }
                    });
                }
            });
            //Add extra space between each date summary
            let rowCount = sheet.rowCount;
            let index = 4;
            let count = 0;
            while (index < rowCount + count) {
                sheet.spliceRows(index, 0, [], []);
                //1st date starts from 4th row 
                index = index + 4;
                count = count + 2;
            }
            let queryParamDateMoment = (0, moment_1.default)(validQueryParams.date, 'YYYY-MM-DD');
            //Above ledger table
            sheet.addTable({
                name: 'MyTable2',
                ref: 'A1',
                headerRow: false,
                columns: columns,
                rows: [
                    ['Payroll Summary - Per Employee', '', '', '', '', ''],
                    [''],
                    [`${subscriberName}`, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'W', ' Work time - All time checked into a work order'],
                    [``, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'B', ' Break time - Paid breaks'],
                    [`Date : ${queryParamDateMoment.format('MM/DD/YYYY')} (${queryParamDateMoment.format('dddd')})`, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'L', ' Lunch time - Unpaid Meal Periods'],
                    ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'B ', ' Break time - Unpaid breaks'],
                    ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'D', ' Drive time'],
                    ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'H1', ' Hour 1']
                ]
            });
            sheet.getRow(1).font = { size: 20, bold: true };
            //Ledger table
            for (let i = 3; i < 9; i++) {
                sheet.mergeCells(`A${i}:R${i}`);
                let row = sheet.getRow(i);
                let firstCell = row.getCell(1);
                firstCell.alignment = {
                    vertical: 'justify', horizontal: 'justify', readingOrder: 'ltr'
                };
                //Cell number 25 of every row
                let cell = row.getCell(25);
                let cellColour = BACKGROUND_COLOR_CODE;
                cell.alignment = {
                    vertical: 'middle', horizontal: 'center'
                };
                if (cell.value == 'W') {
                    cellColour = WORK_EVENT_COLOR_CODE;
                }
                else if (cell.value == 'B') {
                    cellColour = PAID_BREAK_EVENT_COLOR_CODE;
                }
                else if (cell.value == 'L') {
                    cellColour = LUNCH_EVENT_COLOR_CODE;
                }
                else if (cell.value == 'B ') {
                    cellColour = UNPAID_BREAK_EVENT_COLOR_CODE;
                }
                else if (cell.value == 'H1') {
                    cellColour = REGULAR_TIME_COLOR_CODE;
                }
                else if (cell.value == 'D') {
                    cellColour = DRIVE_TIME_EVENT_COLOR_CODE;
                }
                cell.fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    fgColor: {
                        argb: cellColour
                    }
                };
            }
            // save excel file in tmp folder of lambda
            yield workbook.xlsx.writeFile(`/tmp/temporary.xlsx`);
        });
    }
    /**************************************************************/
    /**
     * Return base64 string of created excel saved in lambda tmp folder
     *
     * @returns {string} Base64 string of excel which will be sent as output of lambda
     */
    static getBase64OfPdfConvertedFromExcel() {
        return __awaiter(this, void 0, void 0, function* () {
            //Join the libreoffice source code specified path segments from layer into one path.
            const inputPath = path.join('/opt', 'lo.tar.br');
            try {
                // Decompressing
                let decompressed = {
                    file: yield lambdafs.inflate(inputPath)
                };
            }
            catch (error) {
                console.log('Error while decompressing :----', error);
            }
            //Name on which excel file will be saved in the tmp folder of layer
            let excelFileName = `temporary.xlsx`;
            //Using libreoffice layer imported from lambda layer
            const convertCommand = `export HOME=/tmp && /tmp/lo/instdir/program/soffice.bin --headless --norestore --invisible --nodefault --nofirststartwizard --nolockcheck --nologo --convert-to "pdf:writer_pdf_Export" --outdir /tmp /tmp/${excelFileName}`;
            try {
                execSync(convertCommand).toString('utf8');
            }
            catch (exception) {
                execSync(convertCommand).toString('utf8');
            }
            let fileParts = excelFileName.slice(0, excelFileName.lastIndexOf(".")) + ".pdf";
            let fileB64data = fs.readFileSync('/tmp/' + fileParts);
            return fileB64data.toString('base64');
        });
    }
}
exports.TimesheetExcel = TimesheetExcel;
//# sourceMappingURL=createExcel.js.map