"use strict";
/******************************************************************************
 *
 * Copyright (c) 2019-2021 ServiceActivator Inc. All Rights Reserved.
 *
 * Unauthorized copying of the code in this file, completely or partially,
 * via any medium is strictly prohibited.
 *
 * Proprietary and Confidential.
 *
 * Written by 'Keshav Gupta' <keshav.gupta@wakencode.com>, February 2021
 *
 *****************************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
exports.ERROR_CODES = void 0;
exports.ERROR_CODES = {
    UNAUTHORIZED: "Unauthorized",
    TIMESHEET_NOT_ENABLED: 'Timesheet not enabled for the account',
    TIMESHEET_RECORDS_NOT_FOUND: 'Timesheet record does not exist',
    TIMESHEET_DATA_NOT_FOUND: 'No timesheet data found',
    TIMESHEET_CONFIGS_NOT_FOUND: 'Timesheet configs does not exist',
    INVALID_DATE: 'Invalid date',
    NO_USERS_FOUND: 'No user record found',
    INVALID_WORKDAYS_FOR_GIVEN_RANGE: 'Invalid workdays found for given date range'
};
//# sourceMappingURL=constants.js.map